# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to modify top-level dependencies and run
# './contrib/refresh-requirements.sh to regenerate this file

APScheduler==3.9.1
Pillow==9.2.0
certifi==2022.6.15
charset-normalizer==2.1.0
cycler==0.11.0
decorator==5.1.1
ephem==4.1.3
fonttools==4.34.4
h5py==3.7.0
idna==3.3
kiwisolver==1.4.4
matplotlib==3.5.2
numpy==1.23.1
packaging==21.3
pyparsing==3.0.9
python-dateutil==2.8.2
python-dotenv==0.20.0
pytz-deprecation-shim==0.1.0.post0
pytz==2022.1
requests==2.28.1
sentry-sdk==1.6.0
six==1.16.0
tzdata==2022.1
tzlocal==4.2
urllib3==1.26.10
validators==0.20.0
